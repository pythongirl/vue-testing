# vue-testing
[![pipeline status](https://gitlab.com/pythondude325/vue-testing/badges/master/pipeline.svg)](https://gitlab.com/pythondude325/vue-testing/commits/master)

[View the website](https://pythondude325.gitlab.io/vue-testing)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
