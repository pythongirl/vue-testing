import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

class Route {
  public component: () => Promise<object>
  constructor(public path: string, public name: string, component: Promise<object>){
    this.component = () => component
  }
}

let routes = [
  new Route('/', 'home', import(/* webpackChunkName: "views" */ './views/Home.vue')),
  new Route('/about', 'about', import(/* webpackChunkName: "views" */ './views/About.vue')),
  new Route('/param-test/:text', 'param-test', import(/* webpackChunkName: "views" */ './views/ParamTest.vue')),
]

export default new Router({
  routes
})